$(document).ready(function () {
    // TODO: Your code here

    //1. radio buttons allow user to select different backgrounds

    $('input[type=radio]').change(function() {
        //the below will give you a string value of id for the radio button selected
        var idName = $(this).attr("id");
        var backgroundNum = idName.charAt(idName.length-1);
        var pathToBackImg = "../images/background" + backgroundNum;
        if (backgroundNum == 6) {
            pathToBackImg += ".gif";
        } else {
            pathToBackImg += ".jpg";
        }       
        $("#background").attr("src", pathToBackImg);  
    });

    // 2. The checkboxes should enable the user to choose which dolphin/orca images should be displayed on the selected background.

    $('input[type=checkbox]').change(function() {
        var idName = $(this).attr("id");
        var dolphinImageId = "#" + idName.substring(6);
        console.log("is the checkbox checked: " + $("#" + idName).prop('checked'))
        //.prop('checked') will return false if user unchecks the box, true if user checks the box
        if ($("#" + idName).prop('checked')) {
            $(dolphinImageId).show();
        } else {
            $(dolphinImageId).hide();
        }
    });

    // 3. The range slider controls should permit the user to alter the size and position of the displayed dolphin images.
    
    var xPos = 0;
    var yPos = 0;
    var scaledSize = 1;

    function transAndScale() {
        $('.dolphin').each(function() {
            $(this).css({
                transform: 'translateX(' + xPos + ') translateY(' + yPos + ') scale(' + scaledSize + ', ' + scaledSize + ')'
            })
        });
    }

    $('input[type=range]').change(function() {
        var whichRange = $(this).attr("id");
        var rangeValue = $(this).val();

        if (whichRange == 'horizontal-control') {
            xPos = rangeValue + "px";
            transAndScale();
        } else if (whichRange == 'vertical-control') {
            yPos = rangeValue + "px";
            transAndScale();
        } else {
            if (rangeValue < 0) {
                var reduceScaleNum = (100 + parseInt(rangeValue))/100;
                scaledSize = reduceScaleNum.toFixed(2); 
                transAndScale();
            } else {
                var increaseScaleNum = 1 + (parseInt(rangeValue)/100);
                scaledSize = increaseScaleNum.toFixed(2);
                transAndScale();
            }
        }
    });

});
